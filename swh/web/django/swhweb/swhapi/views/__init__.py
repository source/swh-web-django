from . import api


def get_url_patterns():
    return api.get_url_patterns()
