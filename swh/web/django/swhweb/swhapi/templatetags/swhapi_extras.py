import re

from docutils.core import publish_parts
from docutils.writers.html4css1 import Writer, HTMLTranslator
from inspect import cleandoc

from django import template
from django.utils.html import escape, format_html
from django.utils.safestring import mark_safe


from pygments import highlight
from pygments.lexers import JsonLexer
from pygments.formatters import HtmlFormatter

register = template.Library()


class NoHeaderHTMLTranslator(HTMLTranslator):
    """
    Docutils translator subclass to customize the generation of HTML
    from reST-formatted docstrings
    """
    def __init__(self, document):
        super().__init__(document)
        self.body_prefix = []
        self.body_suffix = []

    def visit_bullet_list(self, node):
        self.context.append((self.compact_simple, self.compact_p))
        self.compact_p = None
        self.compact_simple = self.is_compactable(node)
        self.body.append(self.starttag(node, 'ul', CLASS='docstring'))


DOCSTRING_WRITER = Writer()
DOCSTRING_WRITER.translator_class = NoHeaderHTMLTranslator


@register.filter
def safe_docstring_display(docstring):
    """
    Utility function to htmlize reST-formatted documentation in browsable
    api.
    """
    docstring = cleandoc(docstring)
    return publish_parts(docstring, writer=DOCSTRING_WRITER)['html_body']


@register.filter
def urlize_api_links(text):
    """Utility function for decorating api links in browsable api.

    Args:
        text: whose content matching links should be transformed into
        contextual API or Browse html links.

    Returns
        The text transformed if any link is found.
        The text as is otherwise.

    """
    return re.sub(r'(/api/[^"<]*/|/browse/.*/)',
                  r'<a href="\1">\1</a>',
                  text)


@register.filter
def urlize_header_links(text):
    """Utility function for decorating headers links in browsable api.

    Args
        text: Text whose content contains Link header value

    Returns:
        The text transformed with html link if any link is found.
        The text as is otherwise.

    """
    return re.sub(r'<(/api/.*|/browse/.*)>', r'<<a href="\1">\1</a>>',
                  text)


@register.filter
def highlight_json(text):
    return mark_safe(highlight(text, JsonLexer(), HtmlFormatter()))
