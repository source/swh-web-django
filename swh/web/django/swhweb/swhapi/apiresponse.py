
import json

from rest_framework.response import Response

from swh.storage.exc import StorageDBError, StorageAPIError

from . import utils
from .exc import NotFoundExc, ForbiddenExc


def make_api_response(request, rv, doc_data={}, options={}):
    if request.accepted_media_type == 'text/html':
        doc_env = doc_data
        if rv:
            data = json.dumps(rv, sort_keys=True,
                              indent=4,
                              separators=(',', ': '))
        else:
            data = None
        doc_env['response_data'] = data
        doc_env['headers_data'] = None
        if 'headers' in options:
            doc_env['headers_data'] = options['headers']

        doc_env['request'] = request
        doc_env['heading'] = utils.shorten_path(str(request.path))
        doc_env['status_code'] = options.get('status', 200)
        return Response(doc_env, status=doc_env['status_code'], 
                        template_name='apidoc.html')
    else:
        return Response(rv)


def error_response(request, error, doc_data):
    """Private function to create a custom error response.

    """
    error_code = 400
    if isinstance(error, NotFoundExc):
        error_code = 404
    elif isinstance(error, ForbiddenExc):
        error_code = 403
    elif isinstance(error, StorageDBError):
        error_code = 503
    elif isinstance(error, StorageAPIError):
        error_code = 503

    error_opts = {'status': error_code}
    error_data = {
        'exception': error.__class__.__name__,
        'reason': str(error),
    }

    return make_api_response(request, error_data, doc_data,
                             options=error_opts)
