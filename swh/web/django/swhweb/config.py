from swh.core import config
from swh.storage import get_storage

DEFAULT_CONFIG = {
    'storage': ('dict', {
        'cls': 'remote',
        'args': {
            'url': 'http://127.0.0.1:5002/',
        },
    }),
    'log_dir': ('string', '/tmp/swh/log'),
    'debug': ('bool', None),
    'host': ('string', '127.0.0.1'),
    'port': ('int', 5004),
    'secret_key': ('string', 'development key'),
    'max_log_revs': ('int', 25),
    'limiter': ('dict', {
        'global_limits': ['60 per minute'],
        'headers_enabled': True,
        'strategy': 'moving-window',
        'storage_uri': 'memory://',
        'storage_options': {},
        'in_memory_fallback': ['60 per minute'],
    }),
}

conf = None

def read_config(config_file):
    """Read the configuration file `config_file`"""
    global conf
    conf = config.read(config_file, DEFAULT_CONFIG)
    config.prepare_folders(conf, 'log_dir')
    conf['storage'] = get_storage(**conf['storage'])

    return conf

def storage():
    return conf['storage']
